package com.dolphin.service;

import com.dolphin.commons.Pager;
import com.dolphin.directive.DirectivePage;
import com.dolphin.model.Photos;

import java.io.File;
import java.util.HashMap;


public interface PhotosService {


    /**
     * @description 相册分页
     * @author dolphin
     * @date 2021/11/15 16:10
     */
    Pager<Photos> list(Pager<Photos> pager);

    /**
     * @description 添加相册
     * @author dolphin
     * @date 2021/11/15 16:10
     */
    void addPhotos(Photos photos);

    /**
     * @description 删除相册
     * @author dolphin
     * @date 2021/11/15 16:10
     */
    int del(String id);

    /**
     * @description 根据相册id获取相册信息
     * @author dolphin
     * @date 2021/11/15 16:29
     */
    Photos getById(long id);

    /**
     * @param photos photos
     * @return int  int
     * @description 更新
     * @author dolphin
     */
    int update(Photos photos);

    /**
     * @param photos 相册
     * @return java.io.File
     * @description 压缩相册为zip
     * @author dolphin
     */
    File getPhotosZip(Photos photos);

    DirectivePage<HashMap<String, String>> frontArticlesPage(DirectivePage<HashMap<String, String>> photosPage);

}
