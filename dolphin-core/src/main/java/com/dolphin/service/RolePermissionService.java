package com.dolphin.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.dolphin.model.RolePermission;

/**
 * 描述:
 * ${DESCRIPTION}
 *
 * @author 林平
 * @create 2022-09-22 23:36
 * 个人博客地址：https://www.nonelonely.com
 */
public interface RolePermissionService extends IService<RolePermission> {

}
