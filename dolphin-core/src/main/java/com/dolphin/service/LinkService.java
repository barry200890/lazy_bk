package com.dolphin.service;

import com.dolphin.commons.Pager;
import com.dolphin.directive.DirectivePage;
import com.dolphin.model.Link;

import java.util.HashMap;

/**
 * @description LinkService
 * @author dolphin
 * @date 2021/11/15 10:18
 */
public interface LinkService {
    Pager<Link> list(Pager<Link> pager);

    int add(Link link);

    Link getById(String id);

    int update(Link link);

    int del(String[] idArr);

    DirectivePage<HashMap<String, String>> frontList(DirectivePage<HashMap<String, String>> linkPage);
}
