package com.dolphin.service;

import com.dolphin.model.Database;

/**
 * @description InstallService
 * @author dolphin
 * @date 2021/11/15 10:18
 */
public interface InstallService {
    int addDatabase(Database database) throws Exception;
}
