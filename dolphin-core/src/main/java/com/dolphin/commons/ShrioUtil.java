package com.dolphin.commons;

import com.dolphin.model.User;
import com.dolphin.service.UserService;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.PrincipalCollection;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.BeanUtils;

/**
 * 描述:
 * ${DESCRIPTION}
 *
 * @author 林平
 * @create 2022-09-13 23:23
 * 个人博客地址：https://www.nonelonely.com
 */
public class ShrioUtil {

    public static User getLoginUser() {
        Subject subject = SecurityUtils.getSubject();
        User user = new User();
        PrincipalCollection principals = subject.getPrincipals();
        if (principals == null) {
            return null;
        }
        BeanUtils.copyProperties(principals.getPrimaryPrincipal(), user);
        UserService bean = SpringBeanUtils.getBean(UserService.class);
        User user1 = bean.getById(user.getId().toString());
        return user1;
    }
}
