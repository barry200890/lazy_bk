package com.dolphin.commons;

import org.springframework.stereotype.Component;

import java.lang.annotation.*;

/**
 * 描述:
 * ${DESCRIPTION}
 *
 * @author 林平
 * @create 2022-09-18 12:39
 * 个人博客地址：https://www.nonelonely.com
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Inherited
@Component
public @interface ScheduledTaskAnnotation {

    /**
     * 是否一开机就启动 0 否 1 是
     */
    int initStartFlag() default 0;

    String taskDesc() default "";

    String taskCron() default "0 15 10 15 * ?";
}
