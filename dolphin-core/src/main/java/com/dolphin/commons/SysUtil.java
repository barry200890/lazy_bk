package com.dolphin.commons;

import cn.hutool.core.io.FileUtil;
import cn.hutool.core.util.CharsetUtil;
import cn.hutool.setting.dialect.Props;

import java.io.File;

/**
 * 描述:
 * ${DESCRIPTION}
 *
 * @author 林平
 * @create 2022-09-18 10:28
 * 个人博客地址：https://www.nonelonely.com
 */
public class SysUtil {
    /**
     * 获取安装进度
     *
     * @return String
     */
    public static String getInstallStatus() {
        File file = new File(Constants.DB_PROPERTIES_PATH);
        if (!file.exists()) {
            return null;
        }
        Props dbSetting = new Props(FileUtil.touch(file), CharsetUtil.CHARSET_UTF_8);
        return dbSetting.getStr("installStatus");
    }

    public static boolean isWindows() {
        String os = System.getProperty("os.name");
        if (os.toLowerCase().startsWith("win")) {  //如果是Windows系统
            return true;
        } else {  //linux 和mac
            return false;
        }
    }
}
