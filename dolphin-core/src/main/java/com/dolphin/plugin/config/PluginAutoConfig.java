package com.dolphin.plugin.config;

import com.dolphin.commons.Constants;
import com.dolphin.plugin.PluginManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.nio.file.Paths;

/**
 * 自动配置插件
 * @author dolphin
 */
@Configuration
public class PluginAutoConfig {

    @Bean
    public PluginManager pluginManager() {
        return new PluginManager(Paths.get(Constants.PLUGINS_DIR));
    }
}
