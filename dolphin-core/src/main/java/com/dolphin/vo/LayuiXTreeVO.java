package com.dolphin.vo;

import com.dolphin.model.Permission;
import lombok.Data;
import org.springframework.util.StringUtils;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;

public class LayuiXTreeVO implements Serializable {

    private String id;//节点唯一索引值，用于对指定节点进行各类操作
    private String title;//节点标题
    private String value;
    private String parentId;
    private Boolean checked;//节点是否初始为选中状态（如果开启复选框的话），默认 false
    private Boolean disabled;//节点是否为禁用状态。默认 false
    private Boolean spread;//节点是否初始展开，默认 false
    private List<LayuiXTreeVO> children = new ArrayList<>();
    public LayuiXTreeVO(String title, String value, String id, boolean checked, boolean disabled) {
        this.title = title;
        this.value = value;
        this.id = id;
        this.checked = checked;
        this.disabled = disabled;
        this.parentId = getPid(id);
    }

    public LayuiXTreeVO() {

    }
    private LayuiXTreeVO(String id, String parentId) {
        this.title = id;
        this.value = "";
        this.id = id;
        this.checked = false;
        this.disabled = true;
        this.parentId = parentId;
        this.spread = true;
    }


    private static String getPid(String gp) {
        if (StringUtils.isEmpty(gp)) {
            throw new IllegalArgumentException("title 字段不能为空！");
        } else {
            String[] groupArray = gp.split(":");
            int length = groupArray.length;
            if (length == 1) {
                return "root";
            } else {
                return gp.substring(0, gp.lastIndexOf(":"));
            }
        }
    }

    private static LayuiXTreeVO findChildren(LayuiXTreeVO treeNode, List<LayuiXTreeVO> treeNodes) {
        for (LayuiXTreeVO it : treeNodes) {
            if (treeNode.getId().equals(it.getParentId())) {
                if (treeNode.getChildren() == null) {
                    treeNode.setChildren(new ArrayList<>());
                }
                treeNode.getChildren().add(findChildren(it, treeNodes));
            }
        }
        return treeNode;
    }

    /**
     * 使用递归方法建树
     *
     * @param treeNodes
     * @return
     */
    public static List<LayuiXTreeVO> buildByRecursive(List<LayuiXTreeVO> treeNodes) {
        List<LayuiXTreeVO> trees = new ArrayList<>(treeNodes);
        //权限标识是三级的，故此循环两次即可，此处后续可改为动态循环次数
        List<LayuiXTreeVO> newTrees = iteratorIt(iteratorIt(trees));
        List<LayuiXTreeVO> data = new ArrayList<>(20);
        for (LayuiXTreeVO treeNode : newTrees) {
            if ("root".equals(treeNode.getParentId())) {
                data.add(findChildren(treeNode, newTrees));
            }
        }
        return data;
    }

    private static List<LayuiXTreeVO> iteratorIt(List<LayuiXTreeVO> data) {
        List<LayuiXTreeVO> trees = new ArrayList<>(data);
        data.forEach(d -> {
            if (!"root".equals(d.getParentId())) {
                LayuiXTreeVO xTree = new LayuiXTreeVO(d.getParentId(), getPid(d.getParentId()));
                long cnt1 = trees.stream().filter(tree -> tree.getId().equalsIgnoreCase(d.getParentId())).count();
                if (cnt1 == 0) {
                    trees.add(xTree);
                }
            }
        });
        return trees;
    }

    /**
     * 数据库模型转LayuiXTree
     *
     * @param data
     * @param title
     * @param value
     * @param checked
     * @param disabled
     * @return
     */
    public static List<LayuiXTreeVO> transTo(List<Permission> data,
                                             Function<Permission, String> title,
                                             Function<Permission, Integer> value,
                                             Function<Permission, String> group,
                                             Function<Permission, Boolean> checked,
                                             Function<Permission, Boolean> disabled) {

        List<LayuiXTreeVO> treeList = new ArrayList<>();
        data.forEach(res -> {
            LayuiXTreeVO tree = new LayuiXTreeVO(
                    title.apply(res),
                    value.apply(res).toString(),
                    group.apply(res),
                    checked.apply(res),
                    disabled.apply(res));
            treeList.add(tree);
        });
        return treeList;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getParentId() {
        return parentId;
    }

    public void setParentId(String parentId) {
        this.parentId = parentId;
    }

    public Boolean getChecked() {
        return checked;
    }

    public void setChecked(Boolean checked) {
        this.checked = checked;
    }

    public Boolean getDisabled() {
        return disabled;
    }

    public void setDisabled(Boolean disabled) {
        this.disabled = disabled;
    }

    public Boolean getSpread() {
        return spread;
    }

    public void setSpread(Boolean spread) {
        this.spread = spread;
    }

    public List<LayuiXTreeVO> getChildren() {
        return children;
    }

    public void setChildren(List<LayuiXTreeVO> children) {
        this.children = children;
    }
}
