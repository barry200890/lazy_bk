﻿layui.use(['jquery', 'util'], function () {
    var $ = layui.jquery,
        util = layui.util;
    //导航控制
    master.start($);
    buildCTable($);
    util.fixbar();
});
var slider = 0;
var pathname = window.location.pathname.replace('Read', 'Article');
var master = {};
master.start = function ($) {
    $('#nav li').hover(function () {
        $(this).addClass('current');
    }, function () {
        var href = $(this).find('a').attr("href");
        if (pathname.indexOf(href) == -1) {
            $(this).removeClass('current');
        }
    });
    selectNav();
    function selectNav() {
        var navobjs = $("#nav a");
        $.each(navobjs, function () {
            var href = $(this).attr("href");
            if (pathname.indexOf(href) != -1) {
                $(this).parent().addClass('current');
            }
        });
    };
    $('.phone-menu').on('click', function () {
        $('#nav').toggle(500);
    });
};

function buildCTable($) {
    let hs = $(".artiledetail").find("h1,h2,h3,h4,h5,h6");
    if (hs.length < 2) return;
    let s = "";
    s += '<div class="roundDiv" >';
    s += '<div class="f-floor-title">文章目录</div>';
    s += '<ol">';
    let old_h = 0, ol_cnt = 0;
    for (let i = 0; i < hs.length; i++) {
        let h = parseInt(hs[i].tagName.substr(1), 10);
        if (!old_h) old_h = h;
        if (h > old_h) {
            s += "<ol>";
            ol_cnt++;
        } else if (h < old_h && ol_cnt > 0) {
            s += "</ol>";
            ol_cnt--;
        }
        if (h === 1) {
            while (ol_cnt > 0) {
                s += "</ol>";
                ol_cnt--;
            }
        }
        old_h = h;
        let tit = hs.eq(i).text().replace(/^[\d.、\s]+/g, "");
        tit = tit.replace(/[^a-zA-Z0-9_\-\s\u4e00-\u9fa5]+/g, "");
        if (tit.length < 100) {
            //将每一个h标签拼接到s上，生成目录
            s += '<li><a data-href="#t' + i + '">' + tit + "</a></li>";
            //给文章中的h标签加上id
            hs.eq(i).html('<a name="t' + i + '" id="t' + i + '"></a>' + hs.eq(i).html());
        }
    }
    while (ol_cnt > 0) {
        s += "</ol>";
        ol_cnt--;
    }
    s += "</ol></div>";
    s += '<div style="clear:both"></div>';
    $(".f-floor").append(s);
}
