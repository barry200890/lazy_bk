
window.Win10_parent = parent.Win10; //获取父级Win10对象的句柄
if (!window.Win10_parent) {
    window.Win10_parent = parent.parent.Win10;
}
window.Win10_child = {
    close: function () {
        var index = parent.layer.getFrameIndex(window.name); //先得到当前iframe层的索引
        var layerParent = sessionStorage.getItem("layerParent:" + index);
        var iframe = Win10_parent.getLayeroByIndex(layerParent).find('iframe');
        iframe.queryTable();
        Win10_parent._closeWin(index);
    },
    newMsg: function (title, content, handle_click) {
        Win10_parent.newMsg(title, content, handle_click)
    },
    openUrl: function (url, title, max) {
        var currentIndex = parent.layer.getFrameIndex(window.name); //先得到当前iframe层的索引
        var click_lock_name = Math.random();
        Win10_parent._iframe_click_lock_children[click_lock_name] = true;
        var index = Win10_parent.openUrl(url, title, max, currentIndex);
        setTimeout(function () {
            delete Win10_parent._iframe_click_lock_children[click_lock_name];
        }, 1000);
        return index;
    }
};

layui.use(['layer'], function () {
    $("#file").click(function () {
        var index = Win10_child.openUrl('/admin/attach/choose', "选择图片");
        //监听本地存储 RISKID 键 值
        window.addEventListener('storage',function (e) {
            //获取被修改的键值
            if (e.key == 'sendChooseFile'+index) {
                chooseImageCallback(e.newValue);
            }
        },false);
    });
});
// 取消
$(".p-cancel-btn").click(function () {
    const index = parent.layer.getFrameIndex(window.name);
    parent.layer.close(index);
});


