let form, element, layer, $;
layui.use(['layer', 'form', 'element', 'jquery', 'dtree', 'iconPicker','xmSelect'], function () {
    form = layui.form;
    element = layui.element;
    layer = layui.layer;
    $ = layui.jquery;
    var dtree = layui.dtree;
    var iconPicker = layui.iconPicker;
    var xmSelect = layui.xmSelect;
    // 表单验证
    form.verify({});
    // 表单提交
    form.on('submit(addForm)', function (data) {
        $.ajax({
            type: "POST",
            url: "/admin/menu/add",
            contentType: "application/json",
            data: JSON.stringify(data.field),
            success: function (data) {
                if (data.code === 200) {
                    parent.queryTable();
                    parent.parent.toast.success({message: '添加成功', position: 'topCenter'});
                    const index = parent.layer.getFrameIndex(window.name);
                    parent.layer.close(index);
                    parent.parent.Win10.setDesktop({
                        url: '/admin/menu/getAdminMenu',
                        dom: '#win10-shortcuts'
                    });
                } else {
                    parent.parent.toast.error({message: data.msg, position: 'topCenter'});
                }
            },
            error: function (data) {
                parent.parent.toast.error({message: "添加失败", position: 'topCenter'});
            }
        });
        return false;
    });

    // 取消
    $(".p-cancel-btn").click(function () {
        const index = parent.layer.getFrameIndex(window.name);
        parent.layer.close(index);
    });
    dtree.renderSelect({
        elem: "#pid",
        width: "100%", // 指定树的宽度
        url: "/admin/menu/list",
        method: 'get',
        dataStyle: 'layuiStyle',
        selectInitVal: window.selectInitVal,
        response: {           // 树返回的json格式
            statusName: "code",		  //返回标识
            statusCode: 200,		  //返回码
            message: "message",		  //返回信息
            rootName: "data",		  //根节点名称
            treeId: "id",			  //节点ID
            parentId: "pid",	  //父节点ID
            title: "name",			  //节点名称
            childName: "childMenu",	  //子节点名称
        }
    });
    iconPicker.render({
        // 选择器，推荐使用input
        elem: '#icon',
        // 数据类型：fontClass/unicode，推荐使用fontClass
        type: 'fontClass',
        // 是否开启搜索：true/false，默认true
        search: true,
        // 是否开启分页：true/false，默认true
        page: true,
        // 每页显示数量，默认12
        limit: 12,
        // 每个图标格子的宽度：'43px'或'20%'
        cellWidth: '43px',
        // 点击回调
        click: function (data) {
            console.log(data);
        },
        // 渲染成功后的回调
        success: function (d) {
            console.log(d);
        }
    });
    var demo5 = xmSelect.render({
        el: '#demo1',
        name: 'roles',
        data: []
    });
    $.ajax({
        type: "get",
        url: "/admin/menu/getRoles",
        success: function (data) {
            if (data.code === 200) {
                demo5.update({
                    data: data.data
                });
            } else {
                parent.parent.toast.error({message: data.msg, position: 'topCenter'});
            }
        },
        error: function (data) {
            parent.parent.toast.error({message: "获取失败", position: 'topCenter'});
        }
    });
});

