let util, table;
layui.use(['util', 'layer', 'table'], function () {
    util = layui.util;
    //layer = layui.layer;
    table = layui.table;

    queryTable();
    // 查询
    $("#queryBtn").click(function () {
        queryTable();
    });
    $("#taskBtn").click(function () {
        parent.Win10.openUrl("/admin/scheduled", "任务调度")
    });
    // 添加
    $("#addBtn").click(function () {
        layer.confirm('确定要备份吗?', {icon: 3, title: '提示'}, function (index) {
            $.ajax({
                type: "get",
                url: "/admin/dataBaseBackup/mysqlBackups",
                success: function (data) {
                    if (data.code === 200) {
                        layer.msg(data.msg, {icon: 1});
                    } else {
                        layer.msg(data.msg, {icon: 2});
                    }
                    queryTable();
                },
                error: function (data) {
                    layer.msg("备份失败", {icon: 2});
                    queryTable();
                }
            });
            layer.close(index);
        });
    });
});

/**
 * 查询表格数据
 */
function queryTable() {
    table.render({
        elem: '#tableBox',
        url: '/admin/dataBaseBackup/list',
        method: 'post',
        headers: {'Content-Type': 'application/json'},
        contentType: 'application/json',
        title: '数据库备份列表',
        totalRow: false,
        limit: 10,
        where: {
            form: {
                backupsName: $("#backupsName").val() == '' ? null : $("#backupsName").val()
            }
        },
        cols: [[
            {field: 'id', title: 'ID', width: 80, sort: true},
            {field: 'mysqlIp', minWidth: 160, title: '数据库IP'},
            {field: 'mysqlPort', minWidth: 80, title: '数据库端口'},
            {field: 'mysqlCmd', minWidth: 160, title: '备份命令'},
            {field: 'databaseName', minWidth: 160, title: '数据库名称'},
            {field: 'backupsPath', minWidth: 160, title: '备份数据地址'},
            {field: 'backupsName', minWidth: 160, title: '备份文件名称'},
            {
                field: 'status', minWidth: 100, title: '状态类型', templet: function (d) {
                    let html;
                    if (d.status === 0) {
                        html = "备份中";
                    } else if (d.status === 1) {
                        html = "成功";
                    } else {
                        html = "失败";
                    }
                    return html;
                }
            },
            {
                field: 'id', title: '操作', width: 200,
                templet: function (d) {
                    let html = "<div>";
                    if (d.status === 1) {
                        html = html + "<a class='layui-btn layui-btn-normal layui-btn-xs' onclick='editData(\"" + d.id + "\")'>还原</a> ";
                    }
                    html = html + "<a class='layui-btn layui-btn-normal layui-btn-xs' href='" + '/admin/dataBaseBackup/download/' + d.id + "'>下载</a> ";
                    html = html + "<a class='layui-btn layui-btn-danger layui-btn-xs' onclick='deleteData(\"" + d.id + "\")'>删除</a>" +
                        "</div>";
                    return html;

                }
            }
        ]],
        page: true,
        response: {statusCode: 200},
        parseData: function (res) {
            return {
                "code": res.code,
                "msg": res.msg,
                "count": res.total,
                "data": res.data
            };
        },
        request: {
            pageName: 'pageIndex',
            limitName: 'pageSize'
        }
    });
}

/**
 *
 * @param ids
 */
function deleteData(id) {
    layer.confirm('确定要删除吗?', {icon: 3, title: '提示'}, function (index) {
        $.ajax({
            type: "POST",
            url: "/admin/dataBaseBackup/del",
            contentType: "application/json",
            data: id,
            success: function (data) {
                if (data.code === 200) {
                    queryTable();
                    layer.msg(data.msg, {icon: 1});
                } else {
                    layer.msg(data.msg, {icon: 2});
                }
            },
            error: function (data) {
                layer.msg("删除失败", {icon: 2});
            }
        });
        layer.close(index);
    });
}

function downData(id) {
    layer.confirm('确定要下载吗?', {icon: 3, title: '提示'}, function (index) {
        $.ajax({
            type: "get",
            url: "/admin/dataBaseBackup/download/" + id,
            success: function (data) {
                if (data.code === 200) {
                    queryTable();
                    layer.msg(data.msg, {icon: 1});
                } else {
                    layer.msg(data.msg, {icon: 2});
                }
            },
            error: function (data) {
                layer.msg("下载失败", {icon: 2});
            }
        });
        layer.close(index);
    });
}

/**
 * 还原
 * @param id
 */
function editData(id) {
    layer.confirm('确定要还原吗?', {icon: 3, title: '提示'}, function (index) {
        $.ajax({
            type: "POST",
            url: "/admin/dataBaseBackup/rollback",
            contentType: "application/json",
            data: id,
            success: function (data) {
                if (data.code === 200) {
                    queryTable();
                    layer.msg(data.msg, {icon: 1});
                } else {
                    layer.msg(data.msg, {icon: 2});
                }
            },
            error: function (data) {
                layer.msg("删除失败", {icon: 2});
            }
        });
        layer.close(index);
    });
}

/**
 * 编辑
 * @param id
 */
function editAuth(id) {
    layer.open({
        title: "权限角色",
        type: 2,
        area: common.layerArea($("html")[0].clientWidth, 500, 600),
        shadeClose: true,
        anim: 1,
        content: '/admin/role/authPage/' + id
    });
}
