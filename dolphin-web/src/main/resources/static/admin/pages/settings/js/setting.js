let layer,form,element,toast;
var ws = null;
layui.use(['layer','form','element','toast'], function() {
    layer = layui.layer;
    form = layui.form;
    element = layui.element;
    toast = layui.toast;
    form.on('submit(optionForm2)', function(data){
        save(data);
        return false;
    });
    form.on('submit(optionForm)', function(data){
        save(data);
        return false;
    });
});

function save(data) {
    $.ajax({
        type: "POST",
        url: "/admin/setting/save",
        contentType:"application/json",
        data: JSON.stringify(data.field),
        success:function(d){
            if (d.code === 200){
                parent.toast.success({message: "保存成功",position: 'topCenter'});
            } else {
                parent.toast.error({message: d.msg,position: 'topCenter'});
            }
        },
        error: function (data) {
            parent.toast.error({message: "保存失败",position: 'topCenter'});
        }
    });
}

function sendTestMail(){
    layer.prompt({title: '请输入收件人邮箱', formType: 3}, function(mail, index){
        layer.close(index);
        $.post("/admin/setting/testMail",{mail: mail},function(data,status){
            if (data.code === 200) {
                parent.toast.success({message: "发送成功",position: 'topCenter'});
            } else {
                parent.toast.error({message: data.msg,position: 'topCenter'});
            }
        });
    });
    return false;
}
