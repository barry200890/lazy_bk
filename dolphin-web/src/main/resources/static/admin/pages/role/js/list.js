let util,table;
layui.use(['util', 'layer','table'], function () {
    util = layui.util;
    //layer = layui.layer;
    table = layui.table;

    queryTable();
    // 查询
    $("#queryBtn").click(function () {
        queryTable();
    });

    // 添加
    $("#addBtn").click(function () {
        layer.open({
            title: "添加角色",
            type: 2,
            area: common.layerArea($("html")[0].clientWidth, 500, 400),
            shadeClose: true,
            anim: 1,
            content: '/admin/role/addPage'
        });
    });

});

/**
 * 查询表格数据
 */
function queryTable() {
    table.render({
        elem: '#tableBox',
        url: '/admin/role/list',
        method: 'post',
        headers: {'Content-Type': 'application/json'},
        contentType: 'application/json',
        title: '角色列表',
        totalRow: false,
        limit: 10,
        where: {
            form: {
                name: $("#name").val() == ''?null:$("#name").val()
            }
        },
        cols: [[
            {field: 'id', title: 'ID', width: 80, sort: true},
            {field: 'name',   minWidth: 160,title: '角色名称'},
            {field: 'description',   minWidth: 260,title: '角色描述'},
            {field: 'code',   minWidth: 160,title: '角色编码'},
            {
                field: 'createTime',
                title: '创建时间',
                sort: true,
                minWidth: 160,
                templet: "<span>{{d.createTime ==null?'':layui.util.toDateString(d.createTime, 'yyyy-MM-dd HH:mm:ss')}}</span>"
            },
            {
                field: 'id', title: '操作', width: 200,
                templet: function (d) {
                    if (d.code != 'admin') {
                        let html = "<div>" +
                            "<a class='layui-btn layui-btn-normal layui-btn-xs' onclick='editData(\"" + d.id + "\")'>编辑</a> " +
                            "<a class='layui-btn layui-btn-normal layui-btn-xs' onclick='editAuth(\"" + d.id + "\")'>权限</a> " +
                            "<a class='layui-btn layui-btn-danger layui-btn-xs' onclick='deleteData(\"" + d.id + "\")'>删除</a>" +
                            "</div>"
                        return html;
                    }else {
                        return '管理员有最高权限';
                    }
                }
            }
        ]],
        page: true,
        response: {statusCode: 200},
        parseData: function (res) {
            return {
                "code": res.code,
                "msg": res.msg,
                "count": res.total,
                "data": res.data
            };
        },
        request: {
            pageName: 'pageIndex',
            limitName: 'pageSize'
        }
    });
}

/**
 *
 * @param ids
 */
function deleteData(id) {
    layer.confirm('确定要删除吗?', {icon: 3, title: '提示'}, function (index) {
        $.ajax({
            type: "POST",
            url: "/admin/role/del",
            contentType: "application/json",
            data: id,
            success: function (data) {
                if (data.code === 200) {
                    queryTable();
                    layer.msg(data.msg, {icon: 1});
                } else {
                    layer.msg(data.msg, {icon: 2});
                }
            },
            error: function (data) {
                layer.msg("删除失败", {icon: 2});
            }
        });
        layer.close(index);
    });
}

/**
 * 编辑
 * @param id
 */
function editData(id) {
    layer.open({
        title: "更新角色",
        type: 2,
        area: common.layerArea($("html")[0].clientWidth, 500, 400),
        shadeClose: true,
        anim: 1,
        content: '/admin/role/editPage/'+ id
    });
}

/**
 * 编辑
 * @param id
 */
function editAuth(id) {
    layer.open({
        title: "权限角色",
        type: 2,
        area: common.layerArea($("html")[0].clientWidth, 500, 600),
        shadeClose: true,
        anim: 1,
        content: '/admin/role/authPage/'+ id
    });
}
