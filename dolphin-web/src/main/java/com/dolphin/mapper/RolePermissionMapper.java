package com.dolphin.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.dolphin.model.Permission;
import com.dolphin.model.RolePermission;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * 描述:
 * ${DESCRIPTION}
 *
 * @author 林平
 * @create 2022-09-22 23:34
 * 个人博客地址：https://www.nonelonely.com
 */
@Mapper
@Component
public interface RolePermissionMapper extends BaseMapper<RolePermission> {

    void deleteRolePermissionByUrl(List<Permission> permissions);

}
