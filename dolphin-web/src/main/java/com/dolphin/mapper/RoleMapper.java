package com.dolphin.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.dolphin.model.Role;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Component;

import java.util.List;

@Mapper
@Component
public interface RoleMapper extends BaseMapper<Role> {
    /**
     * 获取角色列表
     *
     * @param role 角色
     * @return List<Role>
     */
    List<Role> list(Role role);

    Role getRoleByCode(String roleCode);

    Role getRoleById(Long id);

    int add(Role role);

}
