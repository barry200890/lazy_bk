package com.dolphin.directive;

import cn.hutool.http.HtmlUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.dolphin.commons.Constants;
import com.dolphin.commons.DateUtils;
import com.dolphin.model.Carousel;
import com.dolphin.service.ArticleService;
import com.dolphin.service.CarouselService;
import com.jfinal.template.Env;
import com.jfinal.template.expr.ast.ExprList;
import com.jfinal.template.io.Writer;
import com.jfinal.template.stat.Scope;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;

/**
 * 描述:
 * ${DESCRIPTION}
 *
 * @author 林平
 * @create 2022-10-06 19:42
 * 个人博客地址：https://www.nonelonely.com
 */
@TemplateDirective("carousels")
@Component
public class CarouselsDirective extends BaseDirective{
    private static CarouselService carouselService;

    @Autowired
    public void setArticleService(CarouselService carouselService){
        CarouselsDirective.carouselService = carouselService;
    }
    @Override
    public void setExprList(ExprList exprList) {
        super.setExprList(exprList);
    }

    @Override
    public void exec(Env env, Scope scope, Writer writer) {
        QueryWrapper<Carousel> wrapper = new QueryWrapper();
        String title = getModelDataToStr("title", scope);
        if (StringUtils.isNotBlank(title)) {
            wrapper.eq("title", title);
        }
        String isOverTime = getModelDataToStr("isOverTime", scope);
        if (StringUtils.isNotBlank(isOverTime)) {
            wrapper.ge("overTime", DateUtils.getNowDate());;
        }
        List<Carousel> carouselList = carouselService.list(wrapper);
        scope.set("carousels", carouselList);
        stat.exec(env, scope, writer);
    }

    @Override
    public boolean hasEnd() {
        return true;
    }
}
