package com.dolphin.controller.api;

import cn.hutool.crypto.SecureUtil;
import com.dolphin.base.BaseApiController;
import com.dolphin.commons.Constants;
import com.dolphin.commons.Pager;
import com.dolphin.commons.ResponseBean;
import com.dolphin.file.FileDownLoad;
import com.dolphin.model.Photo;
import com.dolphin.model.Photos;
import com.dolphin.service.PhotoService;
import com.dolphin.service.PhotosService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.io.File;

@RestController
@CrossOrigin
@Api(value = "相册相关", tags = "相册相关")
@RequestMapping("/api/photos")
public class PhotosController extends BaseApiController {

    @Autowired
    private PhotosService photosService;
    @Autowired
    private PhotoService photoService;

    /**
     * @param pager pager
     * @return com.dolphin.commons.Pager<com.photos.model.Photos>
     * @description 相册分页
     * @author dolphin
     */
    @RequestMapping("/list")
    @ApiOperation(value = "相册分页", notes = "相册分页")
    public Pager<Photos> list(@RequestBody Pager<Photos> pager) {
        return photosService.list(pager);
    }


    /**
     * 添加相册
     *
     * @return String
     */
    @PostMapping("/addPhotos")
    @ApiOperation(value = "添加相册", notes = "添加相册")
    public ResponseBean addPhotos(@RequestBody @Valid Photos photos) {
        try {
            if (photos.getIsEncryption() == Constants.PHOTOS_ENCRYPTION && StringUtils.isBlank(photos.getPassword())) {
                return ResponseBean.fail("请填写相册密码", null);
            }
            photos.setUserId(getUser().getId());
            photosService.addPhotos(photos);
            return ResponseBean.success("添加成功", photos);
        } catch (Exception e) {
            return ResponseBean.fail("添加失败", e.getMessage());
        }
    }

    /**
     * 删除相册
     *
     * @return String
     */
    @PostMapping("/del")
    @ApiOperation(value = "删除相册", notes = "删除相册")
    public ResponseBean del(@RequestBody String id) {
        if (photosService.del(id) > 0) {
            return ResponseBean.success("删除成功", null);
        }
        return ResponseBean.fail("删除失败", null);
    }

    /**
     * 更新相册
     *
     * @return String
     */
    @PostMapping("/editPhotos")
    @ApiOperation(value = "更新相册", notes = "更新相册")
    public ResponseBean editPhotos(@RequestBody @Valid Photos photos) {
        if (photos.getIsEncryption() == Constants.PHOTOS_ENCRYPTION && StringUtils.isNotBlank(photos.getPassword())) {
            photos.setPassword(SecureUtil.md5(photos.getPassword()));
        }
        if (photosService.update(photos) > 0) {
            return ResponseBean.success("更新成功", null);
        }
        return ResponseBean.fail("更新失败", null);
    }

    /**
     * 下载相册
     *
     * @param response response
     * @param id       id
     * @return String
     */
    @GetMapping("/download/{id}")
    @ApiOperation(value = "下载相册", notes = "下载相册")
    public String downloadPhotos(HttpServletResponse response, @PathVariable("id") String id) {
        Photos photos = photosService.getById(Long.parseLong(id));
        File file = photosService.getPhotosZip(photos);
        if (file != null && file.exists()) {
            if (file != null && file.exists()) {
                if (FileDownLoad.fileDownLoad(response, file)) return "下载成功";
            }
        }
        return "下载失败";
    }


    @PostMapping("/photoList")
    @ApiOperation(value = "下载相册", notes = "下载相册")
    public Pager<Photo> photoList(@RequestBody Pager<Photo> pager) {
        Photos photosById = photosService.getById(pager.getForm().getPhotosId());
        if (photosById.getIsEncryption() == Constants.PHOTOS_ENCRYPTION) {
            if (StringUtils.isBlank(pager.getForm().getPassword())) {
                pager.setCode(Pager.ERROR_CODE);
                pager.setMsg("请输入访问密码");
                return pager;
            }
            pager.getForm().setPassword(SecureUtil.md5(pager.getForm().getPassword()));
            if (!pager.getForm().getPassword().equals(photosById.getPassword())) {
                pager.setCode(Pager.ERROR_CODE);
                pager.setMsg("密码错误");
                return pager;
            }
        }
        return photoService.list(pager);
    }
}
