package com.dolphin.controller.api;

import com.dolphin.base.BaseApiController;
import com.dolphin.commons.Pager;
import com.dolphin.commons.ResponseBean;
import com.dolphin.model.Carousel;
import com.dolphin.service.CarouselService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@CrossOrigin
@Api(value = "轮播相关", tags = "轮播相关")
@RequestMapping("/api/carousel")
public class CarouselController extends BaseApiController {

    @Autowired
    private CarouselService carouselService;


    /**
     * @param pager pager
     * @return com.dolphin.commons.Pager<com.carousel.model.carousel>
     * @description 轮播分页
     * @author dolphin
     */
    @RequestMapping("/list")
    @ApiOperation(value = "轮播分页", notes = "轮播分页")
    public Pager<Carousel> list(@RequestBody Pager<Carousel> pager) {
        return carouselService.list(pager);
    }


    /**
     * 添加轮播
     *
     * @return String
     */
    @PostMapping("/add")
    @ApiOperation(value = "添加轮播", notes = "添加轮播")
    public ResponseBean addCarousel(@RequestBody @Valid Carousel carousel) {
        try {
            carouselService.saveOrUpdate(carousel);
            return ResponseBean.success("添加成功", carousel);
        } catch (Exception e) {
            return ResponseBean.fail("添加失败", e.getMessage());
        }
    }

    /**
     * 删除轮播
     *
     * @return String
     */
    @PostMapping("/del")
    @ApiOperation(value = "删除轮播", notes = "删除轮播")
    public ResponseBean del(@RequestBody String id) {
        if (carouselService.removeById(id)) {
            return ResponseBean.success("删除成功", null);
        }
        return ResponseBean.fail("删除失败", null);
    }

    /**
     * 更新轮播
     *
     * @return String
     */
    @PostMapping("/edit")
    @ApiOperation(value = "更新轮播", notes = "更新轮播")
    public ResponseBean editcarousel(@RequestBody @Valid Carousel carousel) {
        if (carouselService.updateById(carousel)) {
            return ResponseBean.success("更新成功", null);
        }
        return ResponseBean.fail("更新失败", null);
    }

}
