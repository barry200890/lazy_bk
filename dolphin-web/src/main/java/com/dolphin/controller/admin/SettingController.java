package com.dolphin.controller.admin;

import cn.hutool.core.io.file.FileReader;
import com.dolphin.base.BaseController;
import com.dolphin.commons.Constants;
import com.dolphin.commons.FileUtil;
import com.dolphin.commons.OptionCacheUtil;
import com.dolphin.commons.ResponseBean;
import com.dolphin.model.Option;
import com.dolphin.model.Permission;
import com.dolphin.permission.AdminMenu;
import com.dolphin.permission.ApiAuth;
import com.dolphin.service.MailService;
import com.dolphin.service.OptionService;
import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/admin")
public class SettingController extends BaseController {

    @Autowired
    private OptionService optionService;

    @Autowired
    private MailService mailService;

    @GetMapping("/setting")
    @ApiAuth(name = "网站设置管理",  groupId = Constants.ADMIN_MENU_GROUP_SETTING, permission = "admin:setting:index")
    public String index(Model model) {
        return view("static/admin/pages/settings/setting.html");
    }

    /**
     * 邮件模板设置
     */
    @GetMapping("/emailSetting")
    @ApiAuth(name = "邮件模板设置", groupId = Constants.ADMIN_MENU_GROUP_SETTING, permission = "admin:setting:emailSetting")
    public String emailSetting(Model model) {
        List<HashMap<String, String>> param = new ArrayList<>();

        HashMap<String, String> comment = new HashMap<>();
        comment.put("path", "/static/admin/tpl/comment_mail.html");
        comment.put("name", "评论邮件模板");
        param.add(comment);

        HashMap<String, String> findPassword = new HashMap<>();
        findPassword.put("path", "/static/admin/tpl/find_password.html");
        findPassword.put("name", "找回密码邮件模板");
        param.add(findPassword);

        model.addAttribute("templates", param);
        return view("static/admin/pages/settings/email_setting.html");
    }

    /**
     * 获取邮件模板文件内容
     */
    @PostMapping("/emailSetting/getFileContent")
    @ResponseBody
    @ApiAuth(name = "获取邮件模板文件内容", permission = "admin:emailSetting:getFileContent")
    public ResponseBean getFileContent(@RequestParam("path") String path){
        return ResponseBean.success("数据加载成功", getEmailTpl(path));
    }

    /**
     * 保存邮件模板
     */
    @PostMapping("/emailSetting/saveFileContent")
    @ResponseBody
    @ApiAuth(name = "获取邮件模板文件内容", permission = "admin:emailSetting:saveFileContent")
    public ResponseBean saveFileContent(@RequestParam("path") String path, @RequestParam("content") String content){
        updateEmailTpl(path, content);
        return ResponseBean.success("文件保存成功", null);
    }

    /**
     * 保存设置信息
     * @param param param
     * @return ResponseBean
     */
    @PostMapping("/setting/save")
    @ResponseBody
    @ApiAuth(name = "保存设置信息", permission = "admin:setting:save")
    public ResponseBean saveSetting(@RequestBody HashMap<String, String> param) {
        List<Option> options = new ArrayList<>();
        for(Map.Entry<String, String> entry : param.entrySet()){
            Option option = new Option();
            option.setOptionKey(entry.getKey());
            option.setOptionValue(entry.getValue());
            options.add(option);
        }
        if (optionService.addOrUpdateOptions(options) > 0) {
            return ResponseBean.success("保存成功", null);
        }
        return ResponseBean.fail("保存失败", null);
    }

    /**
     * 发送测试邮件
     * @param mail mail
     * @return ResponseBean
     */
    @PostMapping("/setting/testMail")
    @ResponseBody
    @ApiAuth(name = "发送测试邮件", permission = "admin:setting:testMail")
    public ResponseBean testMail(String mail) {
        try{
            if (mailService.sendMail(mail, "这是一封测试邮件~", "来自["+ OptionCacheUtil.getValue("WEB_NAME")+"]站点的新消息")) {
                return ResponseBean.success("发送成功", null);
            }
            return ResponseBean.fail("发送失败", null);
        }catch (Exception e) {
            return ResponseBean.fail("邮箱服务出错,发送失败:"+ e.getMessage(), null);
        }
    }

    private String getEmailTpl(String path){
        File file = new File(Constants.PROD_RESOURCES_PATH + path);
        if (!file.exists()) {
            file = FileUtil.getClassPathFile(Constants.DEV_RESOURCES_PATH + path);
        }
        assert file != null;
        FileReader fileReader = new FileReader(file.getAbsolutePath());
        return fileReader.readString();
    }

    private void updateEmailTpl(String path, String content){
        try{
            File file = new File(Constants.PROD_RESOURCES_PATH + path);
            if (!file.exists()) {
                file = FileUtil.getClassPathFile(Constants.DEV_RESOURCES_PATH + path);
            }
            assert file != null;
            OutputStreamWriter write = new OutputStreamWriter(new FileOutputStream(file.getAbsoluteFile()), StandardCharsets.UTF_8);
            BufferedWriter writer = new BufferedWriter(write);
            writer.write(content);
            writer.flush();
            writer.close();
        }catch (Exception e) {
            e.printStackTrace();
        }
    }
}
