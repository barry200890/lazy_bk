package com.dolphin.controller.admin;


import com.dolphin.base.BaseController;
import com.dolphin.commons.Constants;
import com.dolphin.commons.Pager;
import com.dolphin.commons.ResponseBean;
import com.dolphin.model.Option;
import com.dolphin.model.Permission;
import com.dolphin.permission.ApiAuth;
import com.dolphin.service.OptionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * option controlle
 */
@Controller
@RequestMapping("/admin")
public class OptionController extends BaseController {
    @Autowired
    private OptionService optionService;

    @RequestMapping("/option")
    @ApiAuth(name = "配置管理", permission = "admin:option:index", groupId = Constants.ADMIN_MENU_GROUP_SETTING)
    public String index(Model model) {
        return view("static/admin/pages/option/list.html");
    }

    @RequestMapping("/option/addPage")
    @ApiAuth(name = "增加配置页面", permission = "admin:option:addPage", type = Permission.ResType.NAV_LINK)
    public String addPage() {
        return view("static/admin/pages/option/add.html");
    }

    @RequestMapping("/option/updatePage/{id}")
    @ApiAuth(name = "更新配置页面", permission = "admin:option:updatePage", type = Permission.ResType.NAV_LINK)
    public String updatePage(@PathVariable("id") String id, Model model) {
        Option option = optionService.getById(id);
        model.addAttribute("model", option);
        return view("/static/admin/pages/option/update.html");
    }

    /**
     * @param pager pager
     * @return
     * @description 配置分页
     * @author dolphin
     */
    @PostMapping("/option/list")
    @ApiAuth(name = "配置分页", permission = "admin:option:list")
    @ResponseBody
    public Pager<Option> list(@RequestBody Pager<Option> pager) {
        return optionService.list(pager);
    }

    /**
     * 添加配置
     *
     * @return String
     */
    @PostMapping("/option/add")
    @ResponseBody
    @ApiAuth(name = "添加配置", permission = "admin:option:add")
    public ResponseBean add(@RequestBody @Valid Option option) {
        try {
            Option oldOption = optionService.getOptionByKey(option.getOptionKey());
            if (oldOption != null) {
                return ResponseBean.fail("配置编码已存在", null);
            }
            optionService.saveOrUpdate(option);
            return ResponseBean.success("添加成功", option);
        } catch (Exception e) {
            return ResponseBean.fail("添加失败", e.getMessage());
        }
    }

    /**
     * 删除配置
     *
     * @return String
     */
    @PostMapping("/option/del")
    @ResponseBody
    @ApiAuth(name = "删除配置", permission = "admin:option:del")
    public ResponseBean del(@RequestBody String id) {

        if (optionService.removeById(id)) {
            return ResponseBean.success("删除成功", null);
        }
        return ResponseBean.fail("删除失败", null);
    }

    /**
     * 更新配置
     *
     * @return String
     */
    @PostMapping("/option/update")
    @ResponseBody
    @ApiAuth(name = "更新配置", permission = "admin:option:update")
    public ResponseBean editcarousel(@RequestBody @Valid Option option) {

        if (optionService.updateValueByKey(option) > 0) {
            return ResponseBean.success("更新成功", null);
        }
        return ResponseBean.fail("更新失败", null);
    }

    /**
     * 获取配置
     *
     * @return String
     */
    @GetMapping("/option/getOptionByKey")
    @ResponseBody
    public ResponseBean getOptionByKey(String optionKey) {
        return ResponseBean.success("更新成功", optionService.getOptionByKey(optionKey));
    }

}
