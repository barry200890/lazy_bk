package com.dolphin.controller.admin;


import com.dolphin.base.BaseController;
import com.dolphin.commons.Constants;
import com.dolphin.commons.Pager;
import com.dolphin.commons.ResponseBean;
import com.dolphin.model.AccessLogs;
import com.dolphin.permission.ApiAuth;
import com.dolphin.service.AccessLogsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;
import java.util.List;

@Controller
@RequestMapping("/admin")
public class AccessLogsController extends BaseController {

    @Autowired
    private AccessLogsService accessLogsService;

    @RequestMapping("/visit/index")
    @ApiAuth(groupId = Constants.ADMIN_MENU_GROUP_LOG, name = "访问统计", permission = "admin:visit:index")
    public String index() {
        return view("static/admin/pages/visit/index.html");
    }

    @RequestMapping("/visit/getAccessCountByWeek")
    @ResponseBody
    @ApiAuth(name = "周统计", permission = "admin:visit:getAccessCountByWeek")
    public ResponseBean getAccessCountByWeek() {
        HashMap<String, List<Object>> result = accessLogsService.getAccessCountByWeek();
        return ResponseBean.success("", result);
    }

    @RequestMapping("/visit/getAccessCountBySysGroup")
    @ResponseBody
    @ApiAuth(name = "分系统统计", permission = "admin:visit:getAccessCountBySysGroup")
    public ResponseBean getAccessCountBySysGroup() {
        return ResponseBean.success("", accessLogsService.getAccessCountBySysGroup());
    }

    @RequestMapping("/visit/list")
    @ResponseBody
    @ApiAuth(name = "访问列表", permission = "admin:visit:list")
    public Pager<AccessLogs> list(@RequestBody Pager<AccessLogs> pager) {
        return accessLogsService.list(pager);
    }
}
