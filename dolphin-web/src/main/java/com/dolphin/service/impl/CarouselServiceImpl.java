package com.dolphin.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.dolphin.commons.Pager;
import com.dolphin.mapper.CarouselMapper;
import com.dolphin.model.Carousel;
import com.dolphin.service.CarouselService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 描述:
 * ${DESCRIPTION}
 *
 * @author 林平
 * @create 2022-09-06 23:10
 * 个人博客地址：https://www.nonelonely.com
 */
@Service
public class CarouselServiceImpl extends ServiceImpl<CarouselMapper, Carousel> implements CarouselService {


    public Pager<Carousel> list(Pager<Carousel> pager) {
        PageHelper.startPage(pager.getPageIndex(), pager.getPageSize());
        QueryWrapper<Carousel> userQueryWrapper = new QueryWrapper<>(pager.getForm());
        List<Carousel> categories = this.list(userQueryWrapper);
        PageInfo<Carousel> pageInfo = new PageInfo<>(categories);
        pager.setTotal(pageInfo.getTotal());
        pager.setData(pageInfo.getList());
        pager.setCode(Pager.SUCCESS_CODE);
        return pager;
    }

}
