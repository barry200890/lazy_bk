package com.dolphin.service.impl;

import com.dolphin.commons.Pager;
import com.dolphin.mapper.PhotoMapper;
import com.dolphin.model.Photo;
import com.dolphin.service.PhotoService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
public class PhotoServiceImpl implements PhotoService {

    @Autowired
    private PhotoMapper photoMapper;

    public void addPhoto(Photo photo) {
        photo.setCreateTime(new Date());
        photoMapper.addPhoto(photo);
    }

    public Pager<Photo> list(Pager<Photo> pager) {
        List<Photo> photos = photoMapper.getList((pager.getPageIndex() - 1) * pager.getPageSize(),pager.getPageSize(),
                pager.getForm().getName(),  pager.getForm().getPhotosId());
        pager.setTotal(photoMapper.getTotal(pager.getForm().getName(),  pager.getForm().getPhotosId()));
        pager.setData(photos);
        pager.setCode(Pager.SUCCESS_CODE);
        return pager;
    }

    public int del(String id) {
        return photoMapper.del(id);
    }

    public Photo getById(long id) {
        return photoMapper.getById(id);
    }

    public int update(Photo photo) {
        photo.setUpdateTime(new Date());
        return photoMapper.update(photo);
    }
}
