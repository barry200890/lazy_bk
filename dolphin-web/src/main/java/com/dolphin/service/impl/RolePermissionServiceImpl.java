package com.dolphin.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.dolphin.mapper.RolePermissionMapper;
import com.dolphin.model.RolePermission;
import com.dolphin.service.RolePermissionService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

/**
 * 描述:
 * ${DESCRIPTION}
 *
 * @author 林平
 * @create 2022-09-22 23:36
 * 个人博客地址：https://www.nonelonely.com
 */
@Service
public class RolePermissionServiceImpl extends ServiceImpl<RolePermissionMapper, RolePermission> implements RolePermissionService {
    private final static Logger LOGGER = LoggerFactory.getLogger(RolePermissionServiceImpl.class);

}
