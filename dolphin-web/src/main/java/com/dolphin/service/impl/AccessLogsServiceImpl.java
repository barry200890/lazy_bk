package com.dolphin.service.impl;


import com.dolphin.commons.DynamicDataSource;
import com.dolphin.commons.Pager;
import com.dolphin.mapper.AccessLogsMapper;
import com.dolphin.model.AccessLogs;
import com.dolphin.service.AccessLogsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@Service
public class AccessLogsServiceImpl implements AccessLogsService {

    @Autowired
    private AccessLogsMapper accessLogsMapper;


    /**
     * 增加访问
     *
     * @param accessLogs accessLogs
     */
    @Override
    public void addAccess(AccessLogs accessLogs) {
        accessLogsMapper.addAccess(accessLogs);
    }

    /**
     * 获取当周访问数据
     *
     * @return
     */
    @Override
    public HashMap<String, List<Object>> getAccessCountByWeek() {
        List<HashMap<String, Object>> countList;
        if (DynamicDataSource.dataSourceType.equals("mysql")) {
            countList = accessLogsMapper.getAccessCountByWeekFromMysql();
        } else {
            countList = accessLogsMapper.getAccessCountByWeek();
        }
        HashMap<String, List<Object>> result = new HashMap<>();
        List<Object> x = new ArrayList<>();
        List<Object> y = new ArrayList<>();
        for (HashMap<String, Object> day : countList) {
            x.add(day.get("date"));
            y.add(day.get("count"));
        }
        result.put("x", x);
        result.put("y", y);
        return result;
    }

    @Override
    public List<HashMap<String, Object>> getAccessCountBySysGroup() {
        return accessLogsMapper.getAccessCountBySysGroup();
    }

    @Override
    public Pager<AccessLogs> list(Pager<AccessLogs> pager) {
        List<AccessLogs> accessLogs = accessLogsMapper.getList((pager.getPageIndex() - 1) * pager.getPageSize(), pager.getPageSize());
        pager.setTotal(accessLogsMapper.getTotal());
        pager.setData(accessLogs);
        pager.setCode(Pager.SUCCESS_CODE);
        return pager;
    }
}
